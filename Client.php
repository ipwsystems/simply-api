<?php

namespace IpwSystems\SimplyApi;

use Exception;
use InvalidArgumentException;
use Symfony\Component\HttpClient\HttpClient;

class Client {
    protected const END_POINT = 'https://api.simply.com/2/';

    /**
     * List of allowed record types.
     *
     * @var array
     */
    protected $allowedTypes = [
        'A',
        'AAAA',
        'ALIAS',
        'CAA',
        'CNAME',
        'LOC',
        'MX',
        'NS',
        'SRV',
        'SSHFP',
        'TLSA',
        'TXT',
    ];

    private string $username;
    private string $apiKey;

    /**
     * Setup client.
     *
     * @param string $username
     * @param string $apiKey
     */
    public function __construct(string $username, string $apiKey) {
        $this->username = $username;
        $this->apiKey = $apiKey;
    }


    /**
     * List all zones
     *
     * @return array{
     *      object: string,
     *      name: string,
     *      autorenew: int,
     *      domain: array{
     *          name: string,
     *          name_idn: string
     *      },
     *      product:array{
     *          id: int,
     *          name: string,
     *          date_created: int,
     *          date_expire: int|null
     *      }
     * }[]
     */
    public function getZones(): array {
        $response = $this->request("GET", "my/products");
        if (200 !== $response['status']) {
            throw new Exception("Error: " . $response['message'], $response['status']);
        }

        return $response['products'];
    }


    /**
     * Get zone record for a domain.
     *
     * @param string $zone
     * @return array{
     *      serial: int,
     *      name: string,
     *      origin: string
     * }
     */
    public function getZone(string $zone): array {
        $response =  $this->request("GET", "my/products/{$zone}/dns");

        if (200 !== $response['status']) {
            throw new Exception("Error: " . $response['message'], $response['status']);
        }

        return $response['zone'];
    }


    /**
     * Get full list of zone records.
     *
     * @param string $zone
     * @return array{
     *      record_id: int,
     *      name: string,
     *      ttl: int,
     *      data: string,
     *      type: string,
     *      priority: int
     * }[]
     */
    public function getRecords(string $zone): array {
        $response = $this->request("GET", "my/products/{$zone}/dns/records/");
        if (200 !== $response['status']) {
            throw new Exception("Error: " . $response['message'], $response['status']);
        }

        return $response['records'];
    }


    /**
     * Get a single zone record.
     *
     * @param string $zone Domain name for the zone to search.
     * @param string $name name of the record.
     * @param string $type Can be one of: CNAME, NS, A, AAA, MX, TXT
     * @return array{
     *      record_id: int,
     *      name: string,
     *      ttl: int,
     *      data: string,
     *      type: string,
     *      priority: int
     * }|false
     */
    public function getRecord(string $zone, string $name, string $type = '') {
        $name = explode('.', $name)[0];
        $type = strtoupper($type);

        if ($type && !in_array($type, $this->allowedTypes, true)) {
            throw new InvalidArgumentException("Type '{$type}' not supported.");
        }

        foreach ($this->getRecords($zone) as $entry) {
            if ($name === $entry['name']) {
                if (empty($type)) {
                    return $entry;
                }

                if ($type === $entry['type']) {
                    return $entry;
                }
            }
        }

        return false;
    }


    /**
     * Add new DNZ zone record.
     *
     * @param string $zone Domain name of the zone
     * @param string $type Type of record: CNAME, NS, A, AAA, MX, TXT ect.
     *                     Note that the "type" field is case sensetive and must be uppercase.
     * @param array{
     *      name: string,
     *      data: string,
     *      priority: int,
     *      ttl: int
     * } $data DNS record data.
     * @return false|array{
     *      id: int
     * }
     */
    public function addRecord(string $zone, string $type, array $data) {
        $type = strtoupper($type);
        $data['type'] = $type;

        if (!in_array($type, $this->allowedTypes, true)) {
            throw new InvalidArgumentException("Type '{$type}' not supported.");
        }

        $response = $this->request("POST", "my/products/{$zone}/dns/records", ['json' => $data]);
        if (200 === $response['status']) {
            return $response['record'];
        }

        return false;
    }


    /**
     * Update DNS zone record.
     *
     * @param string $zone Domain name of the zone
     * @param integer $recordId ID of the record to update
     * @param string $type Type of record: CNAME, NS, A, AAA, MX, TXT ect.
     *                     Note that the "type" field is case sensetive and must be uppercase.
     * @param array{
     *      name: string,
     *      data: string,
     *      priority: int,
     *      ttl: int
     * } $data DNS record data.
     * @return bool
     */
    public function updateRecord(string $zone, int $recordId, string $type, array $data): bool {
        $type = strtoupper($type);
        $data['type'] = $type;

        if (!in_array($type, $this->allowedTypes, true)) {
            throw new InvalidArgumentException("Type '{$type}' not supported.");
        }

        $response = $this->request("PUT", "my/products/{$zone}/dns/records/{$recordId}", ['json' => $data]);
        return 200 === $response['status'];
    }


    /**
     * Delete a DNS zone record.
     *
     * @param string $zone Domain name of the zone.
     * @param integer $recordId ID of the record to delete.
     * @return bool
     */
    public function deleteRecord(string $zone, int $recordId): bool {
        $response = $this->request("DELETE", "my/products/{$zone}/dns/records/{$recordId}");
        return 200 === $response['status'];
    }


    /**
     * Send http request to api endpoint.
     *
     * @param string $method GET, POST, PUT, DELETE is supported.
     * @param string $endpoint URI for the endpoint to call.
     * @param array|null $options Array of options - if applicable.
     * @return array
     */
    protected function request(string $method, string $endpoint, array $options = []): array {
        $httpClient = HttpClient::createForBaseUri(self::END_POINT, [
            'auth_basic' => "{$this->username}:{$this->apiKey}"
        ]);

        $response = $httpClient->request($method, $endpoint, $options);

        if (200 !== $response->getStatusCode()) {
            $data = $response->toArray(false);
            throw new Exception($data['message'], $data['status']);
        }

        return $response->toArray();
    }
}
