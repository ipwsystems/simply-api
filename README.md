Simple wrapper for the Simply.com API

Api documentation here https://www.simply.com/dk/docs/api/

## Installation

> composer require ipwsystems/simply-api

## Usage:

```php
require_once __DIR__ . '/vendor/autoload.php';

use IpwSystems\SimplyApi\Client;

$c = new Client('<username>', '<api key>');

// List zones.
$c->getZones();

// Get zone info.
$c->getZone('domain.tld');

// List records in zone.
$c->getRecords('domain.tld');

// Get zone specific zone record.
$c->getRecord('domain.tld', '<sub record>');

// Get zone specific zone record explicit type.
$c->getRecord('domain.tld', '<sub record>', 'A');

// Add a new record to a zone.
$c->addRecord('domain.tld', 'A', [
    'name' => 'sub.domain.tld',
    'data' => '1.2.3.4',
    'priority' => 1,
    'ttl' => 21600,
]);

// Update a zone record.
// Where 123 is the record_id for the zone record. - See output from getRecord().
$c->updateRecord('domain.tld', 123, 'A', [
    'name' => 'sub.domain.tld',
    'data' => '1.2.3.4',
    'priority' => 10,
    'ttl' => 600,
]);

// Delete a zone record.
// Where 123 is the record_id for the zone record. - See output from getRecord().
$c->deleteRecord('domain.tld', 123)
```
